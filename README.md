# Vaga de estágio na Lyncas

[![Lyncas Logo](https://img-dev.feedback.house/TCo5z9DrSyX0EQoakV8sJkx1mSg=/fit-in/300x300/smart/https://s3.amazonaws.com/feedbackhouse-media-development/modules%2Fcore%2Fcompany%2F5c9e1b01c5f3d0003c5fa53b%2Flogo%2F5c9ec4f869d1cb003cb7996d)](https://www.lyncas.net)

## Teste

### Uma breve introdução ao conceito de API's

API é uma interface de comunicação muito utilizada na atualidade. É através dela que muitos sistemas se comunicam. Existe um padrão chamado REST que define
alguns pontos para padronizar o desenvolvimento de API's. Vamos exemplificar. Ao acessarmos um site, www.lyncas.net por exemplo, não estamos chamando uma API.
O site da Lyncas é um produto que utiliza um padrão MVC, ou seja possuí Modelos, Visões e Controladores. Uma API não possuí interface de tela. Quando falamos
de API estamos falando de comunicação entre sistemas e por isso não há telas, somente mensagens textuais.

Enquanto o site da Lyncas é uma mistura de HTML e CSS que constróem um visual elegante para o usuário final, também possuí códigos em PHP utilizado no WordPress
que é uma plataforma amplamente utilizada e com componentes prontos que carecem apenas de adaptações.

As API's são formas de troca de mensagem no formato de texto. Para chamar uma API, vamos supor que o endereço da lyncas tivesse uma api tal qual fosse chamada
assim: www.lyncas.net/api/

Abrindo essa URL (endereço) em um navegador, ao invés de carregar uma tela bonita, ele apresentar uma tela em branco. (por exemplo)

Agora vamos supor que abríssemos a URL www.lyncas.net/api/users e o retorno em tela fosse:

```
[
    {
        "first_name": "Pedro",
        "last_name": "Borel"
    },
    {
        "first_name": "Matuzalém",
        "last_name": "Nascimento"
    }
]
```

Estranhamente isso é uma mensagem trocada por meio de API's seguindo o padrão REST. Onde temos na URL o NOME de um objeto de sistema que no caso acima é `users`
fazendo referência a um modelo de dados exemplo Usuários. E pelo resultado podemos subentender que o nosso modelo Usuário possuí 2 valores os quais são Primeiro
nome e Último nome. Também podemos assumir a partir do padrão REST que esse sistema só possuí 2 usuários cadastrados no sistemaa pois o padrão `/api/users` é 
definido para trazer todos os resultados.

### Objetivo

Entender o conceito de uma API e desenvolver uma simples API com no mínimo 1 controller e 1 action. Criar um projeto em uma das linguagens sugeridas abaixo.
Sugestão pode ser uma API de usuários. Não existe nenhum requisito obrigatório exceto o descrito acima. O objetivo é incentivar o aprendizado avulso de alguns
conceitos básicos abordados no desenvolvimento de projetos web. Se você quiser pode reproduzir uma API conforme o exemplo dado na introdução mas isso fica a
seu critério.

> Não é necessário a utilização de Banco de Dados

* _Linguagens sugeridas_: PHP, NodeJS, Ruby, Python, Java, C# .Net
* _Caso não tenha preferência, sugerimos NodeJS pois será a utilizada na Lyncas_

### Prazo de entregaa

- Você tem 7 dias a partir do envio desse teste para você para retornar com o que você conseguiu fazer.
- A nossa sugestão é que você não invista mais de 8 horas em cima desse projeto. 
- Nos apresente onde você conseguiu chegar e tudo que você aprendeu.

### Forma de apresentação/entrega

O teste pode ser enviado num arquivo .zip ou publicado em algum repositório Git (caso o autor possua conhecimento para tal / não é obrigatório)

Será agendado uma data para conversa remota e apresentação do teste.

Não é obrigatório a API funcionar. O objetivo foi dado mas o resultado final será construído a partir da conversa de apresentação.

Será avaliado:

- Dificuldades encontradas
- Conceitos aprendidos (API, REST, MVC, Controller, Actions, ...)
- Explicação do código desenvolvido, porque, como, pra que, ...

## Teste de personalidade

Realizar o teste de personalidade https://br.vonvon.co/VmtcT, tirar um print do resultado (gráfico de competências), copiar o resultado textual do perfil e enviar junto com o teste.

## Carta de apresentação

Escrever em até 2 parágrafos porque gostaria dessa oportunidade.

## Observações

Tanto o teste de personalidade quanto a carta de apresentação devem ser feitas em um único documento no Google Docs e compartilhado com `maykon@lyncas.net`
